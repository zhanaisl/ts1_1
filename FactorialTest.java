import org.example.Factorial;
import org.junit.Assert;
import org.junit.Test;
public class FactorialTest {
    private final Factorial test = new Factorial();

    @Test
    public void test1(){
        Assert.assertEquals(test.factorial(4), 24);
    }

    @Test
    public void test2(){
        Assert.assertEquals(test.factorial(3), 4);
    }

    @Test
    public void test3(){
        Assert.assertEquals(test.factorial(0),1);
    }
}
